from django.contrib import admin
from models import domain, article, site, theme, menu
from django_markdown.admin import MarkdownModelAdmin

class articleAdmin(admin.ModelAdmin):
     prepopulated_fields = {"url": ("title",)}



#class UserAdminManager(models.AdminManager):
#    """
#    Custom manager for the User model.
#    """
#    def get_query_set(self):
#        """
#        Overwrites the get_query_set to only return Users in the queue.
#        """
#        return super(UserAdminManager, self).get_query_set().filter(userprofile__queue=True)

#admin_objects = UserAdminManager()




admin.site.register(domain)
admin.site.register(article, MarkdownModelAdmin)
admin.site.register(site)
admin.site.register(theme)
admin.site.register(menu)

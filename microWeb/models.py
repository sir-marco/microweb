from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class menu(models.Model):
    name            = models.CharField(max_length='40',default='main')
    links           = models.ManyToManyField('microWeb.article')
    enabled         = models.BooleanField()

    def __unicode__(self):
        return self.name


class theme(models.Model):
    name            = models.CharField(max_length=30)
    description     = models.CharField(max_length=200, blank='true')
    preview         = models.ImageField(upload_to="images/",help_text='nahled tematu', max_length=250, blank='true')
    css_path        = models.FileField(upload_to="css_path/", max_length=250)
    template        = models.FileField(upload_to="templates/", max_length=250, blank='true',default = None)
    enabled         = models.BooleanField()

    def __unicode__(self):
        return self.name


class article(models.Model):
    """
    all articles
    """
    order           = models.IntegerField(default=10, help_text="Poradi polozky v menu")
    title           = models.CharField(max_length=15, help_text='Pojmenovani polozky v menu')
    text            = models.TextField(blank=True)
    submenu         = models.ForeignKey(menu, blank = 'true', null=True)
    url             = models.SlugField(max_length=30, help_text="cesta ke strance v url")
    desc            = models.CharField(max_length=800, help_text="meta popisek", blank=True)
    keys            = models.CharField(max_length=200, help_text="klicova slova", blank=True)
    autor           = models.ForeignKey(User)
    created         = models.DateTimeField(default=timezone.now())
    enabled         = models.BooleanField(default='true')

    def __unicode__(self):
        return self.url


class site(models.Model):
    """
    site config
    """
    name            = models.CharField(max_length="15", default="default")
    theme           = models.ForeignKey(theme)
    logo            = models.ImageField(upload_to="images/", max_length=200, blank=True)
    footer_text     = models.CharField("footer text", max_length=150, blank=True)
    enabled         = models.BooleanField(help_text="je stranka povolena")
    owner           = models.ForeignKey(User) 
    articles        = models.ManyToManyField(article, help_text="Priradte clanky k domene", related_name='pages_in_domain')
    default_page    = models.ForeignKey(article, help_text="vyberte stranku, ktera bude pouzita jako vychozi")
    main_menu       = models.ForeignKey(menu)

    def __unicode__(self):
        return self.name


class domain(models.Model):
    """
    domain names and aliases
    """
    name            = models.CharField(max_length=100)
    site            = models.ForeignKey(site)
    enabled         = models.BooleanField()
    owner           = models.ForeignKey(User)

    def __unicode__(self):
        return self.name
